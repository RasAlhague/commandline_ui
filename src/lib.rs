pub mod label_control;
pub mod standard_app;
pub mod generic_input_control;

pub use crate::label_control::LabelControl;
pub use crate::generic_input_control::GenericInputControl;
pub use crate::standard_app::StandardApp;

use std::any::Any;

pub trait UiElement {
    fn name(&self) -> String;
    fn display(&self);
	fn as_any(&self) -> &dyn Any;
}

pub trait UiApp {
    fn get_element(&self, key: &str) -> Option<&Box<dyn UiElement>>;
    fn startup_element(&self) -> String;
	
    fn run(&self) -> Result<(), &'static str> {
		match self.get_element(&self.startup_element()) {
			Some(element) => {
				element.display();
				return Ok(());
			},
			None => Err("No startup element with given name found!"),
		}
	}
}