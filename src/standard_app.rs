use std::collections::HashMap;
use crate::UiApp;
use crate::UiElement;

pub struct StandardApp<'a> {
	ui_elements: HashMap<&'a str, Box<dyn UiElement>>,
	startup_element_name: String,
}

impl<'a> StandardApp<'a> {
	pub fn create_empty() -> StandardApp<'a> {
		StandardApp {
			ui_elements: HashMap::new(),
			startup_element_name: String::from(""),
		}
	}
	
	pub fn create_with_startup(element_name: &'a str, element: Box<dyn UiElement>) -> StandardApp<'a> {
		let mut app = StandardApp {
			ui_elements: HashMap::new(),
			startup_element_name: String::from(element_name.clone()),
		};
		
		app.add_element(element_name, element);
		
		return app;
	}

	pub fn add_element(&mut self, key: &'a str, ui_element: Box<dyn UiElement>){
		if !self.ui_elements.contains_key(key.clone()) {
			self.ui_elements.insert(key, ui_element);
		}
	}
	
    pub fn remove_element(&mut self, key: &str) {
		self.ui_elements.remove(key);
	}
	
    pub fn set_startup_element(&mut self, name: &str) {
		self.startup_element_name = String::from(name);
	}
}

impl UiApp for StandardApp<'_> {
    fn get_element(&self, key: &str) -> Option<&Box<dyn UiElement>> {
		self.ui_elements.get(key)
	}
	
    fn startup_element(&self) -> String{
		self.startup_element_name.clone()
	}
}