use std::io;
use crate::UiElement;
use std::any::Any;

pub struct GenericInputControl<T, E, G>
    where E: Fn(String) -> T,
          G: Fn(T)
{
	name: String,
	text: String,
	input_converter: E,
	input_received: Option<G>,
}

pub fn convert_string_to_u32(val: String) -> u32 {
    3
}

impl<T, E, G> GenericInputControl<T, E, G>
    where E: Fn(String) -> T,
          G: Fn(T)
{
    pub fn create(name: &str, text: &str, converter: E, input_received: G) -> GenericInputControl<T, E, G> {
        GenericInputControl::<T, E, G> {
            name: String::from(name),
            text: String::from(text),
            input_converter: converter,
            input_received: Some(input_received),
        }
    }
    
    pub fn register_handler(&mut self, handler: G) {
        self.input_received = Some(handler);
    }
}

impl<T: 'static, E: 'static, G: 'static> UiElement for GenericInputControl<T, E, G>
    where E: Fn(String) -> T,
          G: Fn(T)
{
    fn name(&self) -> String {
        self.name.clone()
    }
    
    fn display(&self) {
        println!("{}", self.text);
        print!(">>");
        
        let mut input = String::new();

        io::stdin().read_line(&mut input)
            .expect("Failed to read line");
            
        match &self.input_received {
            Some(event) => (event)((self.input_converter)(input)),
            None => return,
        }
    } 

    
    fn as_any(&self) -> &dyn Any {
        self
    }
}