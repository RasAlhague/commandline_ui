use crate::UiElement;
use std::any::Any;

pub struct LabelControl {
    text: String,
    name: String,
    underline_char: char,
    underlined: bool,
}

impl LabelControl {
    pub fn create(name: &str, text: &str, underline_char: char) -> LabelControl {
        LabelControl {
            name: String::from(name),
            text: String::from(text),
            underline_char: underline_char,
            underlined: false,
        }
    }
    
    pub fn create_underlined(name: &str, text: &str, underline_char: char) -> LabelControl{
        let mut label = LabelControl::create(name, text, underline_char);
        label.underlined = true;
        
        return label;
    }
}

impl UiElement for LabelControl {
    fn name(&self) -> String {
        self.name.clone()
    }
    
    fn display(&self) {
        println!("{}", self.text);
        
        if self.underlined {
            for _i in 0..self.text.len() {
                print!("{}", self.underline_char);
            }
            print!("\n");
        }
        
    } 

    fn as_any(&self) -> &dyn Any {
        self
    }
}